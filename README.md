# PF

## Description

Depuis le lancement de SpaceX, les API de l'entreprise subissent de nombreux appelle chaque heure.
Cela engendre de la latence et de l'indisponibilité, l'objecitf du projet est de collecter les données de leur API chaque jour,
et de les hébergers sur notre propre infra avec de nouveaux API.

Pour des résont d'évolution future et de partenaire diverse, le mécanisme de collect va être laborieux.

Les objécitfs sont :
* Collecter toutes les 5 heures les données des API Rockets et Lauch Pads (https://docs.spacexdata.com/?version=latest)
* Mettre à disposition les mêmes API Rockets et Lauch Pads
* Créer une API qui permet de calculer pour chaque Lauch Pad ça distance avec l'ISS (http://api.open-notify.org/iss-now.json)

Un début d'API vous ai fourni, avec une base de données.

L'utilisation de pylint pour l'analyse de code est fortement conseillée. Il a été rajouter task sur la CI du projet.

## Infrastructure
![alt text](infra.png "Infrastructure")
## Taches

### Docker
1. Ajout d'un service Telegraf sur la stack docker (https://hub.docker.com/_/telegraf?tab=description)
2. Ajout d'un service zookeper, et d'un Kafka sur la stack docker(https://hub.docker.com/r/wurstmeister/kafka / https://hub.docker.com/_/zookeeper)
3. Ajout d'un volume dans le service Telegraf, pour ajouter un fichier telegraf.conf
4. Configuration du Telegraf pour call les API SpaceX en in et un Kafka en out (https://docs.influxdata.com/telegraf/v1.14/plugins/plugin-list/#http , https://docs.influxdata.com/telegraf/v1.14/plugins/plugin-list/#kafka)

### Python
5. Faire un listner Python qui écoute les messages du kafka (https://kafka-python.readthedocs.io/en/master/usage.html#kafkaconsumer)
7. Modifier les entities de api/sources/python/entites, pour correspondre au object LaunchPad et Rocket (https://pydantic-docs.helpmanual.io/)
8. Ajouter une connection à un mongo. (https://api.mongodb.com/python/current/tutorial.html)
9. Faire que lors des appels des API, les objets soit rajouter et modifier en base
10. Modifier le listner pour que quand il reçoit un message du kafka, il le redirige sur API
11. En capsuler le listner dans une image est le rajouter à la stack
12. Rajouter une API qui calcul pour chaque Lauch Pad ça distance avec l'ISS

## Build

```bash
#Build app
docker-compose build


#Start app
docker-compose up -d 

#Use port 5001 for API

#Run API without docker
# You can used virtual env if u want https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/
#Install pip package for app
cd api
pip install -r requirements.txt

#Run api
cd sources/python
python -m api.app
```
