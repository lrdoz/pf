"""
File defined context of app
"""

import logging
import os
import yaml
from pymongo import MongoClient

class Context:
    """
    Class defined variable of app
    """
    def __init__(self):
        """
        """
        #Init log
        self.configure_log()

        #Server configuration
        self.host = os.environ.get("HTTP_HOST", default = "0.0.0.0")
        self.port = int(os.environ.get("HTTP_PORT", default = "5001"))
        self.prefix_url = os.environ.get("HTTP_ROOT", default = "/spacex")

        #Project conf
        self.project_name = os.environ.get("PROJECT_NAME", default = "SpaceX API")
        self.version = os.environ.get("VERSION", default = "1.0")

        #Kafka configuration
        self.kafka_host = os.environ.get("KAFKA_HOST", default = "kafka:9092")
        self.kafka_topic = os.environ.get("KAFKA_TOPIC", default = "spacex")

        self.init_mongo_client()

    def configure_log(self):
        """
        Load log configuration for uvicorn serve
        """
        self.log_level = os.environ.get("LOGGING", default = logging.INFO)
        log_conf_path = os.environ.get("LOGGING_CONF", default = "api/log.yaml")

        self.log_conf = yaml.safe_load(open(log_conf_path)) \
            if os.path.exists(log_conf_path) else None

    def init_mongo_client(self):
        """
        Init mongo connector
        """
        self.mongo_host = os.environ.get("MONGO_HOST", default = "mongo")
        self.mongo_port = os.environ.get("MONGO_PORT", default = 27017)
        self.mongo_db = os.environ.get("MONGO_DB", default = "spacex")
        self.mongo = MongoClient(self.mongo_host, self.mongo_port)[self.mongo_db]
