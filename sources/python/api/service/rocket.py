"""
Define Rocket service
"""

from typing import List
from api.entities.rocket import Rocket

class RocketService:
    """
    Class defined
    """

    COLLECTION_NAME = "Rocket"

    def __init__(self, db) -> None:
        self.db_collection = db[self.COLLECTION_NAME]

    def get_rockets(self) -> List[Rocket]:
        """
        """
        result = self.db_collection.find({})

        return  list(map(lambda x: Rocket(**x), result))
