"""
Define LaunchPad service
"""

from typing import List, Tuple
from math import sin, cos, sqrt, atan2, radians

import requests
from api.entities.launch_pad import LauchPad

class LaunchPadService:
    """
    Class defined
    """

    COLLECTION_NAME = "Launchpad"
    ISS_HOST = "http://api.open-notify.org/iss-now.json"

    def __init__(self, db) -> None:
        self.db_collection = db[self.COLLECTION_NAME]

    def get_launch_pads(self) -> List[LauchPad]:
        """
        """
        result = self.db_collection.find({})
        
        launch_pads = list(map(lambda x: LauchPad(**x), result))
        iss_position = self.__get_iss_position()
        if iss_position is None or not launch_pads:
            return launch_pads
        list(map(lambda launch_pad: self.__dist(launch_pad, iss_position), launch_pads))

        return launch_pads

    @classmethod
    def __get_iss_position(cls):
        response = requests.get(cls.ISS_HOST)
        if not response.ok:
            return None
        positions = response.json()["iss_position"]
        return float(positions["latitude"]), float(positions["longitude"])

    @classmethod
    def __dist(cls, launch_pad: LauchPad, iss_position: Tuple[float, float]):
        R = 6373.0
        lat1 = radians(launch_pad.location_latitude)
        lon1 = radians(launch_pad.location_longitude)
        lat2 = radians(iss_position[0])
        lon2 = radians(iss_position[1])
        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        launch_pad.dist_to_iss = R * c
        return launch_pad
