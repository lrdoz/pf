"""
File describe entites of lauchpad
"""

from typing import Optional
from pydantic import BaseModel

class LauchPad(BaseModel):
    """
    Lauch pad description
    """
    id: int
    attempted_launches: int
    location_latitude: float
    location_longitude: float
    response_time: float
    successful_launches: int
    dist_to_iss: Optional[int]
