"""
File describe entites of rocket
"""

from pydantic import BaseModel
from typing import Optional

class Rocket(BaseModel):
    """
    Rocket description
    """
    id: int
    boosters: int
    cost_per_launch: int
    diameter_feet: float
    diameter_meters: float
    engines_engine_loss_max: Optional[int]
    engines_isp_sea_level: int
    engines_isp_vacuum: int
    engines_number: int
    engines_thrust_sea_level_kN: int
    engines_thrust_sea_level_lbf: int
