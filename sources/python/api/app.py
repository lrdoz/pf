"""
File defined APP server
"""

import uvicorn
from fastapi import FastAPI

from starlette.responses import Response
from starlette.middleware.cors import CORSMiddleware

from context import Context
import api.routes.rocket as rocket
import api.routes.launch_pad as launch_pad

# Get context data and init app
CTX = Context()
APP = FastAPI(docs_url=CTX.prefix_url+"/docs", openapi_url=CTX.prefix_url+"/openapi.json",
              redoc_url=CTX.prefix_url+"/redoc", title=CTX.project_name, version=CTX.version)

#Add conf for allow cross domaine
APP.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

#API product
APP.include_router(
    rocket.ROUTER,
    prefix=CTX.prefix_url+"/rocket",
    tags=["Rocket"]
)

APP.include_router(
    launch_pad.ROUTER,
    prefix=CTX.prefix_url+"/launch_pad",
    tags=["Launch Pad"]
)

@APP.get(CTX.prefix_url+'/version')
def get_version(response: Response):
    """
    Retrieve app version

    :param response: Response object of app
    """
    response.status_code = 200
    return {"version": CTX.version}

if __name__ == "__main__":
    uvicorn.run(APP, host=CTX.host, port=CTX.port, log_config=CTX.log_conf, log_level=CTX.log_level)
