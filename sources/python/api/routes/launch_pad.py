"""
File describe rocket routes on app
"""

from typing import List
from fastapi import APIRouter

from context import Context
from api.entities.launch_pad import LauchPad
from api.service.launch_pad import LaunchPadService

ROUTER = APIRouter()
CTX = Context()

@ROUTER.get("/", description="Retrieve data of launch pad",
            response_model=List[LauchPad])
def retrieve_data():
    """
    Retrieve data on api
    """
    launch_pad = LaunchPadService(CTX.mongo)
    return launch_pad.get_launch_pads()
