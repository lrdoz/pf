"""
File describe rocket routes on app
"""

from typing import List
from fastapi import APIRouter

from context import Context
from api.entities.rocket import Rocket
from api.service.rocket import RocketService

ROUTER = APIRouter()
CTX = Context()

@ROUTER.get("/", description="Retrieve data rocket",
            response_model=List[Rocket],
            response_model_exclude_unset=True)
def retrieve_data():
    """
    Retrieve data on api
    """
    rocket = RocketService(CTX.mongo)
    return rocket.get_rockets()
