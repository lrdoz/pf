"""
Tool used by listner
"""

def next_unflatten(output):
    """
    Recurise unflatten call
    """
    for key, value in output.items():
        if isinstance(value, dict):
            output[key] = unflatten(value)
        elif isinstance(value, list) and isinstance(value[0], dict):
            output[key] = list(map(unflatten, value))
    return output

def unflatten(data, separator="_"):
    """
    FLatten json InfluxDB data to Json format
    """
    output = dict()
    for key, value in data.items():
        elements = key.split(separator)
        new_index = None
        for index, element in enumerate(elements):
            if element.isdigit():
                new_index = index
        if new_index is None:
            output[key] = value
            continue
        new_key = separator.join(elements[:new_index])
        if new_key not in output:
            output[new_key] = list()
        if new_index + 1 == len(elements):
            output[new_key].append(value)
        else:
            output[new_key].append({separator.join(elements[new_index + 1:]):value})
    return next_unflatten(output)
