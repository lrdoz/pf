"""
Kafka listner
"""

import json
import logging
from enum import Enum

from kafka import KafkaConsumer
from context import Context

from listner.tools import unflatten

class MapInputToDB(Enum):
    """
    Enum defined mapping on host call and database collection
    """
    Rocket = "https://api.spacexdata.com/v3/rockets"
    Launchpad = "https://api.spacexdata.com/v3/launchpads"

if __name__ == "__main__":
    LOGGER = logging.getLogger(__name__)

    CTX = Context()

    #Init mongo connection
    DB = CTX.mongo

    #Init kafka connection
    KAFKA_CONSUMER = KafkaConsumer(CTX.kafka_topic,
                                   bootstrap_servers=CTX.kafka_host,
                                   value_deserializer=lambda m: json.loads(m.decode('utf-8')))

    HOSTS = set(map(lambda host: host.value, MapInputToDB))
    for MESSAGE in KAFKA_CONSUMER:
        URL = MESSAGE.value["tags"]["server"]
        if not URL in HOSTS:
            LOGGER.error("Not found host %s", URL)
            continue

        DATA = unflatten(MESSAGE.value["fields"])
        DATA["_id"] = DATA["id"]
        collection = MapInputToDB(URL).name
        DB[collection].delete_one({"_id":DATA["id"]})
        DB[collection].insert_one(DATA)
